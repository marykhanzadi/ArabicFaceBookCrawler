package Processing;

import java.util.Date;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 * @author h.fakour
 */
public class likesPageMeta {

    private String about;
    private String bio;
    private String built;
    private String name;
    private List<String> likes_id;
    private String category;
    private String phone;
    private Date last_used_time;
    private String website;
    private long likes_count;
    private String media_url;
    private String description;
    private String city;
    private String country;
    private String hometownName;
    private String birthday;
    private String gender;
    private String id;
    private Date crawl_date;
    private PythonArabicNormalizer norm;

    public likesPageMeta(Date crawl_date, String phone, long likes_count, String about, String bio,
            String birthday, String built, String name, List<String> likes_id, String website, String category,
            String id, String media_url, String description, Date last_used_time, String city, String country,
            String hometownName) throws Exception {
        this.norm = new PythonArabicNormalizer();
        this.id = id;
        this.about = norm.getTextNormalized(about);
        this.bio = norm.getTextNormalized(bio);
        this.built = norm.getTextNormalized(built);
        this.birthday = birthday;
        this.name = norm.getTextNormalized(name);
        this.likes_id = likes_id;
        this.website = website;
        this.category = category;
        this.likes_count = likes_count;
        this.phone = norm.getTextNormalized(phone);
        this.last_used_time = last_used_time;
        this.media_url = media_url;
        this.description = norm.getTextNormalized(description);
        this.city = norm.getTextNormalized(city);
        this.country = norm.getTextNormalized(country);
        this.hometownName = norm.getTextNormalized(hometownName);
        this.birthday = birthday;
        this.crawl_date = crawl_date;

    }

    public List<String> getLikes_id() {
        return likes_id;
    }

    public void setLikes_id(List<String> likes_id) {
        this.likes_id = likes_id;
    }

    public Date getCrawl_date() {
        return crawl_date;
    }

    public void setWebsite(String website) throws Exception {
        this.website = website;
    }

    public String getWebsite() {
        return website;
    }

    public void setPhone(String phone) throws Exception {
        this.phone = norm.getTextNormalized(phone);
    }

    public String getPhone() {
        return phone;
    }

    public void setName(String name) throws Exception {
        this.name = norm.getTextNormalized(name);
    }

    public String getName() {
        return name;
    }

    public void setMedia_url(String media_url) {

        this.media_url = media_url;
    }

    public String getMedia_url() {
        return media_url;
    }

    public void setLikes_count(long likes_count) {
        this.likes_count = likes_count;
    }

    public long getLikes_count() {
        return likes_count;
    }

    public void setLast_used_time(Date last_used_time) {
        this.last_used_time = last_used_time;
    }

    public Date getLast_used_time() {
        return last_used_time;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setHometownName(String hometownName) throws Exception {
        this.hometownName = norm.getTextNormalized(hometownName);
    }

    public String getHometownName() {
        return hometownName;
    }

    public void setGender(String gender) throws Exception {
        this.gender = norm.getTextNormalized(gender);
    }

    public String getGender() {
        return gender;
    }

    public void setDescription(String description) throws Exception {
        this.description = norm.getTextNormalized(description);
    }

    public String getDescription() {
        return description;
    }

    public void setCountry(String country) throws Exception {
        this.country = norm.getTextNormalized(country);
    }

    public String getCountry() {
        return country;
    }

    public void setCity(String city) throws Exception {
        this.city = norm.getTextNormalized(city);
    }

    public String getCity() {
        return city;
    }

    public void setCategory(String category) throws Exception {
        this.category = norm.getTextNormalized(category);
    }

    public String getCategory() {
        return category;
    }

    public void setBuilt(String built) {
        this.built = built;
    }

    public String getBuilt() {
        return built;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBio(String bio) throws Exception {
        this.bio = norm.getTextNormalized(bio);
    }

    public String getBio() {
        return bio;
    }

    public void setAbout(String about) throws Exception {
        this.about = norm.getTextNormalized(about);
    }

    public String getAbout() {
        return about;
    }

}
