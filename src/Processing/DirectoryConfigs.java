package Processing;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.solr.client.solrj.impl.HttpSolrClient;

/**
 *
 * @author Hamed Fakour
 */
public class DirectoryConfigs {

    public String AccessTokenPost;

    public String ServerUrl;
    public HttpSolrClient Solr;
    public String keywordSearch;
    public String facefriends;
    public String facepost;
    public String facelikes;
    public List<String> configs;
    public Set<String> keywordsSearch;

    public DirectoryConfigs(String config_file_dir) {
        try {
             String current_path = getCurrentPath();
            this.configs = GetConfigurations(config_file_dir);
            this.AccessTokenPost = configs.get(0);
            this.ServerUrl = configs.get(1);
            this.facefriends = configs.get(2);
            this.facelikes = configs.get(3);
            this.facepost = configs.get(4);
            this.keywordSearch = current_path +configs.get(5);

        } catch (Exception ex) {
            Logger.getLogger(DirectoryConfigs.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private List<String> GetConfigurations(String ConfigFileDir) {
        this.configs = ReadConfig(ConfigFileDir);
        return configs;
    }

    private List<String> ReadConfig(String ConfigFileDir) {
        File file = new File(ConfigFileDir);
        List<String> confs = new ArrayList();
        try {
            Scanner sc = new Scanner(file);
            while (sc.hasNext()) {
                confs.add(Splitter(sc.nextLine()));
            }
        } catch (FileNotFoundException ex) {
            System.err.println("The configuration file has problem.");
        }
        return (confs);
    }

    public static String getCurrentPath() throws URISyntaxException {
        URI path = DirectoryConfigs.class.getProtectionDomain().getCodeSource().getLocation().toURI();
        String jar_path = path.getPath();
        return jar_path.replace("FacebookCrawler.jar", "").replace("build/classes/", "");
    }

    private String Splitter(String line) {
        return line.substring(line.indexOf("=") + 1).trim();
    }

}
