package Processing;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author h.fakour
 */
public class PostMeta {

    private String post_message;
    private String fromName;
    private String id;
    private Date created_time;
    private long likes_count;
    private String caption;
    private int comments_count;
    private String media_url;
    private String type_message;
    private Set hashtags = new HashSet();
    private Set mentions = new HashSet();
    private String external_url;
    private String type_attach;
    private String title;
    private String description;
    private String Page_id;
    private Date crawl_date;
    private PythonArabicNormalizer norm;

    public PostMeta(Date crawl_date, String page_id, String FromName, String external_url, String type_attach, String title, String description, Date created_time, String post_message, String caption, long likes_count, int comments_count, String media_url, String type_message, String id, Set Hashtags, Set Mentions) throws Exception {
        this.norm = new PythonArabicNormalizer();
        this.type_attach = type_attach;
        this.Page_id = page_id;
        this.external_url = external_url;
        this.media_url = media_url;
        this.title = norm.getTextNormalized(title);
        this.description = norm.getTextNormalized(description);
        this.fromName = norm.getTextNormalized(FromName);
        this.id = id;
        this.created_time = created_time;
        this.post_message = norm.getTextNormalized(post_message);
        this.caption = norm.getTextNormalized(caption);
        this.likes_count = likes_count;
        this.comments_count = comments_count;
        this.type_message = type_message;
        this.hashtags = norm.getTextNormalized(Hashtags);
        this.mentions = norm.getTextNormalized(Mentions);
        this.crawl_date = crawl_date;

    }

    public Date getCrawl_date() {
        return crawl_date;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getLikes_count() {
        return likes_count;
    }

    public void setLikes_count(long likes_count) {
        this.likes_count = likes_count;
    }

    public String getMedia_url() {
        return media_url;
    }

    public void setMedia_url(String media_url) {
        this.media_url = media_url;
    }

    public Set getMentions() {

        return mentions;
    }

    public void setMentions(Set Mentions) throws Exception {
        PythonArabicNormalizer norm = new PythonArabicNormalizer();
        this.mentions = norm.getTextNormalized(Mentions);
    }

    public String getPage_id() {
        return Page_id;
    }

    public void setPage_id(String Page_id) {
        this.Page_id = Page_id;
    }

    public String getPost_message() {

        return post_message;
    }

    public void setPost_message(String post_message) throws Exception {
        PythonArabicNormalizer norm = new PythonArabicNormalizer();
        this.post_message = norm.getTextNormalized(post_message);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) throws Exception {
        PythonArabicNormalizer norm = new PythonArabicNormalizer();
        this.title = norm.getTextNormalized(title);
    }

    public String getType_message() {
        return type_message;
    }

    public String getType_attach() {
        return type_attach;
    }

    public void setType_attach(String type_attach) {
        this.type_attach = type_attach;
    }

    public void setType_message(String type_message) {
        this.type_message = type_message;
    }

    public String getId() {
        return id;
    }

    public void setHashtags(Set Hashtags) throws Exception {
        PythonArabicNormalizer norm = new PythonArabicNormalizer();
        this.hashtags = norm.getTextNormalized(Hashtags);
    }

    public Set getHashtags() {
        return hashtags;
    }

    public void setFromName(String FromName) throws Exception {
        PythonArabicNormalizer norm = new PythonArabicNormalizer();
        this.fromName = norm.getTextNormalized(FromName);
    }

    public String getFromName() {
        return fromName;
    }

    public void setExtraweb_url(String external_url) {
        this.external_url = external_url;
    }

    public String getExternal_url() {
        return external_url;
    }

    public void setDescription(String description) throws Exception {
        PythonArabicNormalizer norm = new PythonArabicNormalizer();
        this.description = norm.getTextNormalized(description);
    }

    public String getDescription() {
        return description;
    }

    public void setCreated_time(Date created_time) {
        this.created_time = created_time;
    }

    public Date getCreated_time() {
        return created_time;
    }

    public void setComments_count(int comments_count) {
        this.comments_count = comments_count;
    }

    public int getComments_count() {
        return comments_count;
    }

    public void setCaption(String caption) throws Exception {
        PythonArabicNormalizer norm = new PythonArabicNormalizer();
        this.caption = norm.getTextNormalized(caption);
    }

    public String getCaption() {
        return caption;
    }

}
