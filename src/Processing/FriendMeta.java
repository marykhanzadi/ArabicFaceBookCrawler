package Processing;

import com.restfb.types.Comments;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author h.fakour
 */
public class FriendMeta {

    private String hometown_name;
    private String birthday;
    private String bio;
    private String about;
    private String location;
    private String religion;
    private List languages;
    private int age_range;
    private String profile_image;
    private String name;
    private List work;
    private List educations;
    private String quotes;
    private String email;
    private List interested;
    private String relationship_status;
    private String website;
    public String id;
    private String gender;
    private Date crawl_date;
     private PythonArabicNormalizer norm;

    public FriendMeta(Date crawl_date, List languages, String religion, String id, String about, int age_range, List<String> work, List<String> educations, String profile_image, String location, String gender, String quotes, String email, List interested, String name, String relationship_status, String website, String hometown_name, String birthday, String bio) throws Exception {
         this.norm = new PythonArabicNormalizer();
        this.gender =norm.getTextNormalized( gender);
        this.hometown_name =norm.getTextNormalized( hometown_name);
        this.birthday = birthday;
        this.bio = norm.getTextNormalized(bio);
        this.about = norm.getTextNormalized(about);
        this.location = norm.getTextNormalized(location);
        this.religion = norm.getTextNormalized(religion);
        this.languages = norm.getTextNormalized(languages);
        this.age_range = age_range;
        this.profile_image = profile_image;
        this.name = norm.getTextNormalized(name);
        this.work = norm.getTextNormalized(work);
        this.educations = norm.getTextNormalized(educations);
        this.quotes = norm.getTextNormalized(quotes);
        this.email = email;
        this.interested = norm.getTextNormalized(interested);
        this.relationship_status = norm.getTextNormalized(relationship_status);
        this.website = norm.getTextNormalized(website);
        this.id = id;
        this.crawl_date = crawl_date;
    }

    public Date getCrawl_date() {
        return crawl_date;
    }

    public List getInterested() {
        return interested;
    }

    public void setWork(List work) throws Exception {
        if (work != null) {
            PythonArabicNormalizer fr_norm = new PythonArabicNormalizer();
            this.work = fr_norm.getTextNormalized(work);
        } else {
            this.work = work;
        }

    }

    public List getWork() {
        return work;
    }

    public void setWebsite(String website) throws Exception {
        this.website = website;
    }

    public String getWebsite() {
        return website;
    }

    public void setReligion(String religion) throws Exception {
        if (religion != null) {
            PythonArabicNormalizer fr_norm = new PythonArabicNormalizer();
            fr_norm.getTextNormalized(religion);
        } else {
            this.religion = religion;
        }

    }

    public String getReligion() {
        return religion;
    }

    public void setRelationship_status(String relationship_status) throws Exception {
        if (relationship_status != null) {
            PythonArabicNormalizer fr_norm = new PythonArabicNormalizer();
            this.relationship_status = fr_norm.getTextNormalized(relationship_status);
        } else {
            this.relationship_status = relationship_status;
        }

    }

    public String getRelationship_status() {

        return relationship_status;
    }

    public void setQuotes(String quotes) throws Exception {
        if (quotes != null) {
            PythonArabicNormalizer fr_norm = new PythonArabicNormalizer();
            this.quotes = fr_norm.getTextNormalized(quotes);
        } else {
            this.quotes = quotes;
        }

    }

    public String getQuotes() {
        return quotes;
    }

    public void setProfile_image(String profile_image) throws Exception {

        this.profile_image = profile_image;
    }

    public String getProfile_image() {
        return profile_image;
    }

    public void setName(String name) throws Exception {
        if (name != null) {
            PythonArabicNormalizer fr_norm = new PythonArabicNormalizer();
            this.name = fr_norm.getTextNormalized(name);
        } else {
            this.name = name;
        }

    }

    public String getName() {
        return name;
    }

    public void setLocation(String location) throws Exception {
        if (location!=null){
           PythonArabicNormalizer fr_norm = new PythonArabicNormalizer();
        this.location = fr_norm.getTextNormalized(location); 
        }else{
             this.location =location;
        }
        
    }

    public String getLocation() {
        return location;
    }

    public void setLanguages(List languages) throws Exception {
        PythonArabicNormalizer fr_norm = new PythonArabicNormalizer();
        this.languages = fr_norm.getTextNormalized(languages);
    }

    public List getLanguages() {
        return languages;
    }

    public void setInterested(List interested) throws Exception {
        PythonArabicNormalizer fr_norm = new PythonArabicNormalizer();
        this.interested = fr_norm.getTextNormalized(interested);
    }

    public List getInterestedIn() {
        return interested;
    }

    public void setId(String id) {

        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setHometownName(String hometown_name) throws Exception {
        PythonArabicNormalizer fr_norm = new PythonArabicNormalizer();
        this.hometown_name = fr_norm.getTextNormalized(hometown_name);
    }

    public String getHometown_name() {
        return hometown_name;
    }

    public void setGender(String gender) throws Exception {
        PythonArabicNormalizer fr_norm = new PythonArabicNormalizer();
        this.gender = fr_norm.getTextNormalized(gender);
    }

    public String getGender() {
        return gender;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setEducations(List educations) throws Exception {
        PythonArabicNormalizer fr_norm = new PythonArabicNormalizer();
        this.educations = fr_norm.getTextNormalized(educations);
    }

    public List getEducations() {
        return educations;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBio(String bio) throws Exception {
        PythonArabicNormalizer fr_norm = new PythonArabicNormalizer();
        this.bio = fr_norm.getTextNormalized(bio);
    }

    public String getBio() {
        return bio;
    }

    public void setAge_Range(int age_range) {
        this.age_range = age_range;
    }

    public int getAge_Range() {
        return age_range;
    }

    public void setAbout(String about) throws Exception {
        PythonArabicNormalizer fr_norm = new PythonArabicNormalizer();
        this.about = fr_norm.getTextNormalized(about);
    }

    public String getAbout() {
        return about;
    }

}
