/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ParseData;

import Processing.DirectoryConfigs;
import Processing.PostMeta;
import Processing.FriendMeta;
import Processing.Logging;
import Processing.likesPageMeta;
import com.restfb.Connection;
import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;
import com.restfb.Parameter;
import com.restfb.Version;
import com.restfb.exception.FacebookException;
import com.restfb.exception.FacebookGraphException;
import com.restfb.exception.FacebookJsonMappingException;
import com.restfb.exception.FacebookNetworkException;
import com.restfb.exception.FacebookOAuthException;
import com.restfb.exception.FacebookResponseStatusException;
import com.restfb.types.Comment;
import com.restfb.types.Page;
import com.restfb.types.Post;
import com.restfb.types.User;
import com.restfb.types.User.Education;
import com.restfb.types.WorkExperience;
import static java.lang.System.out;
import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Jwan
 */
public class ExtractFields {

    private String profile_image;
    private String post_message;
    private String from_name;
    private String page_id;
    private Date created_time;
    private long likes_count;
    private String caption;
    private int comments_count;
    private String media_url;
    private String type_attach;
    // private Comments comments;
    private String extraweb_url;
    private String type_message;
    private String title;
    private String description;
    private String hometownName;
    private String birthday;
    private String bio;
    private String about;
    private String gender;
    private String location;
    private String city;
    private String country;
    private String religion;
    private List languages;
    private int age_range;
    private List<String> work;
    private List<String> educations;
    private String quotes;
    private String email;
    private List interestedIn;
    // private List Favorite_athletes;
    private String name;
    private String relationship_status;
    private String website;
    private String id;
    private String built;
    private List<String> likes_id;
    private String category;
    private String phone;
    private Date last_used_time;
    public static String accessToken = "EAACW5Fg5N2IBAM0CUZCkEiblnNvxIDHh20IAYXO38CWgzIazOWs7kAAKMOOemV1i2rYkTTkX40QCSPLBk9OUWLMgBb0Gx9iTi7EQq33HXZBZCcEXZAgZATusmPQOMIMDK9UcRblHkk94YF1XLsr3U6ZCQJsPgKGN2fgIVTV8DTgAZDZD";

    public ExtractFields() {
        clear_field();
    }

    private void clear_field() {
        post_message = new String();
        from_name = new String();
        page_id = new String();
        created_time = null;
        likes_count = 0;
        caption = new String();
        comments_count = 0;
        media_url = new String();
        type_attach = new String();
        type_message = new String();
        extraweb_url = new String();
        title = new String();
        description = new String();
        hometownName = new String();
        birthday = new String();
        bio = new String();
        about = new String();
        gender = new String();
        location = new String();
        city = new String();
        country = new String();
        religion = new String();
        languages = new ArrayList<>();
        age_range = 0;
        work = new ArrayList<>();
        educations = new ArrayList<>();
        quotes = new String();
        email = new String();
        interestedIn = new ArrayList<>();

        //Favorite_teams = new String();
        //  Favorite_athletes = new String();
        name = new String();
        relationship_status = new String();
        website = new String();
        id = new String();
        built = new String();
        likes_id = new ArrayList<>();
        category = new String();
        phone = new String();
        last_used_time = null;
    }

    public static void main(String[] args) throws InterruptedException, Exception {
        // System.setProperty("java.net.useSystemProxies", "true");
        //System.setProperty("socksProxyVersion", "4");
        ExtractFields post = new ExtractFields();

        //String date = "Wed May 20 15:04:46 IRDT 2018";
        Calendar calendar = Calendar.getInstance();
        String End_Date = calendar.getTime().toString();
        calendar.add(Calendar.HOUR, -48);
        String Start_Date = calendar.getTime().toString();
        //post.getCommentsPost("195064987941165", accessToken, Start_Date, End_Date, "Page");
        //List<FriendMeta> facebookFriends = post.getFacebookFriends("me", accessToken);
        // post.printlistfriend(facebookFriends);
        //post.getTimelinePost("118194755634", "/feed", accessToken, "6/30/2018 09:59:09 PM", "7/5/2018 04:18:36 PM", "page");
        Set<likesPageMeta> facebookpages = new HashSet<>();
        // Set<likesPageMeta> page = post.getExtraPage("133904593643689", accessToken, facebookpages);
//        for (likesPageMeta next : facebookpages) {
//            System.out.println("get From id :" + next.getId());
//            System.out.println("get From name:" + next.getName());
//            List<PostMeta> timelinePost = post.getTimelinePost(next.getId(), "/feed", accessToken, Start_Date, End_Date, "Page");
////            timelinePost.forEach((nexts) -> {
////                post.getCommentsPost(nexts.getId(), accessToken, Start_Date, End_Date, "Page");
////            });
//        }
    }

    public void getCommentsPost(String ID, String AccessToken, String START_DATE, String End_DATE, String typerequest) {

        FacebookClient facebookClient = new DefaultFacebookClient(AccessToken);

        Connection<Comment> commentConnection
                = facebookClient.fetchConnection(ID + "/comments", Comment.class, Parameter.with("fields", "created_time,message_tags,attachment,from,id,like_count,comment_count,message,is_hidden,is_private"),
                        Parameter.with("until", "now"),
                        Parameter.with("since", START_DATE), Parameter.with("limit", 100));

        //int personalLimit = 50;
        for (List<Comment> commentPage : commentConnection) {
            for (Comment comment : commentPage) {
                out.println("Get Created Time Comments : " + comment.getCreatedTime());
                out.println("Comments Message: " + comment.getMessage());
                System.out.println("Comments LikeCount: " + comment.getLikeCount());
                if (comment.getLikes() != null) {
                    for (int i = 0; i < comment.getLikes().getData().size(); i++) {
                        System.out.println("Name Like " + comment.getLikes().getData().get(0).getName());
                        System.out.println("getType Like " + comment.getLikes().getData().get(0).getType());
                        System.out.println("ID Like " + comment.getLikes().getData().get(0).getId());
                        System.out.println("CreatedTime Like " + comment.getLikes().getData().get(0).getCreatedTime());
                        System.out.println("Metadata Like " + comment.getLikes().getData().get(0).getMetadata());
                    }
                }

                out.println("Comments Id: " + comment.getId());
                if (comment.getAttachment() != null) {
                    System.out.println("URL: " + comment.getAttachment().getUrl());
                    System.out.println("Target: " + comment.getAttachment().getTarget());
                    System.out.println("Media: " + comment.getAttachment().getMedia());
                    System.out.println("Type: " + comment.getAttachment().getType());
                    System.out.println("fb.com/ " + comment.getAttachment().getId());
                    System.out.println("Title " + comment.getAttachment().getTitle());
                    System.out.println("Description " + comment.getAttachment().getDescription());
                }
                System.out.printf("--------------------------------------------------------- \n");
            }
        }
    }

    public List<PostMeta> getTimelinePost(String pageid, String Request, String AccessToken, String START_DATE, String End_DATE, String typerequest) {
        List<PostMeta> listpost = new ArrayList();
        int cnt = 0;
        Logging logs = new Logging();

        try {
//            System.setProperty("java.net.useSystemProxies", "true");
//            setproxy();
            FacebookClient facebookClient = new DefaultFacebookClient(AccessToken, Version.LATEST);

            Connection<Post> PostFeed = facebookClient.fetchConnection(pageid + "/feed", Post.class,
                    Parameter.with("fields", "picture,attachments,caption,actions,comments,likes,message_tags,message,from,link,created_time,admin_creator,description,event,type,child_attachments"),
                    Parameter.with("until", "now"),
                    Parameter.with("since", START_DATE), Parameter.with("limit", 100));

            for (List<Post> PostPage : PostFeed) {
                for (Post post : PostPage) {
                    clear_field();
                    if (post.getFrom() != null) {
                        from_name = post.getFrom().getName();
                    }
                    if (post.getAttachments() != null) {
                        extraweb_url = post.getAttachments().getData().get(0).getUrl();
                        if (post.getAttachments().getData().get(0).getMedia() != null) {
                            media_url = post.getAttachments().getData().get(0).getMedia().getImage().getSrc();
                        }
                        type_attach = post.getAttachments().getData().get(0).getType();
                        title = post.getAttachments().getData().get(0).getTitle();
                        description = post.getAttachments().getData().get(0).getDescription();
                    }
                    //comments = post.getComments();
                    created_time = post.getCreatedTime();
                    post_message = post.getMessage();
                    caption = post.getCaption();
                    likes_count = post.getLikesCount();
                    if (post.getComments() != null) {
                        comments_count = post.getComments().getData().size();
                    }
//                ImageUrl = post.getPicture();
                    type_message = post.getType();
                    id = post.getId();
                    page_id = pageid;
                    Set Hashtags = new HashSet();
                    Set Mentions = new HashSet();
                    Pattern patternStr = Pattern.compile("#(?>\\p{L}\\p{M}*+|_)+", Pattern.UNICODE_CHARACTER_CLASS);
                    if (post_message != null) {
                        Matcher matcher = patternStr.matcher(post_message);
                        while (matcher.find()) {
                            Hashtags.add(matcher.group());
                        }
                        patternStr = patternStr = Pattern.compile("@(?>\\p{L}\\p{M}*+|_)+", Pattern.UNICODE_CHARACTER_CLASS);
                        matcher = patternStr.matcher(post_message);
                        while (matcher.find()) {
                            Mentions.add(matcher.group());
                        }
                    }
                    Date crawl_date = new Date();
                    try {
                        PostMeta newmeta = new PostMeta(crawl_date, page_id, from_name, extraweb_url, type_attach, title, description, created_time, post_message, caption, likes_count, comments_count, media_url, type_message, id, Hashtags, Mentions);
                        listpost.add(newmeta);
                    } catch (Exception ex) {
                        Logger.getLogger(ParseData.ExtractFields.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    System.out.printf("====================================================================================== \n");
                    System.out.println("page id and from : " + page_id + " , " + from_name + " post id: " + id);
                    System.out.println("crawl_date:" + crawl_date);
                    System.out.printf("====================================================================================== \n");
                }
            }
//            System.setProperty("java.net.useSystemProxies", "false");
        } catch (Exception e) {
            System.out.println(logs.LoggingStringDate() + typerequest + "Post: fb.com/" + id + " Error" + e);
//            System.setProperty("java.net.useSystemProxies", "false");
        }
        System.out.println(logs.LoggingStringDate() + typerequest + ": fb.com/" + pageid + " Count Of TimelinePost:" + cnt);
//        System.setProperty("java.net.useSystemProxies", "false");
        return listpost;
    }

    private void printlistpost(List<PostMeta> post) {
        int cnt = 0;
        for (PostMeta en : post) {
            cnt++;
            System.out.println("count naumber:" + cnt);
            System.out.println("getFrom().getName():" + en.getFromName());
            System.out.println("crawl_date:" + en.getCrawl_date());
            System.out.println("Page_id:" + en.getPage_id());
            System.out.println("Extraweb_url: " + en.getExternal_url());
            System.out.println("Type_attach: " + en.getType_attach());
            System.out.println("Title: " + en.getTitle());
            System.out.println("MediaUrl: " + en.getMedia_url());
            System.out.println("Description: " + en.getDescription());
            System.out.println("date:" + en.getCreated_time());
            System.out.println("Message: " + en.getPost_message());
            System.out.println("Mentions:" + en.getMentions().toString());
            System.out.println("Hashtags:" + en.getHashtags().toString());
            System.out.println("Caption:" + en.getCaption());
            System.out.println("Message ID: fb.com/" + en.getId());
            System.out.println("LikesCount: " + en.getLikes_count());
            System.out.println("CommentsCount: " + en.getComments_count());
            System.out.println("Type Message:" + en.getType_message());
        }
    }

    public List<FriendMeta> getFacebookFriends(String UserID, String AccessToken) throws InterruptedException, Exception {
        List<FriendMeta> listfriend = new ArrayList();
        FacebookClient facebookClient = new DefaultFacebookClient(AccessToken, Version.LATEST);
        Logging logs = new Logging();
        try {
            // System.setProperty("java.net.useSystemProxies", "true");
//            setproxy();
            Connection<User> myFriends = facebookClient.fetchConnection(UserID + "/friends", User.class,
                    Parameter.with("fields", "quotes,location,relationship_status,religion,website,work,about,age_range,birthday,email,context,education,first_name,gender,hometown,id,books,interested_in,favorite_teams,favorite_athletes,name,family,short_name,middle_name,last_name,languages,picture"),
                    Parameter.with("limit", "100"));
            System.out.println("User:" + UserID);
            int cnt = 0;
            for (List<User> users : myFriends) {
                for (User user : users) {
                    cnt++;
                    clear_field();
                    Date crawl_date = new Date();
                    getInfoUser(user, crawl_date);
                    FriendMeta newfriend = new FriendMeta(crawl_date, languages, religion, id, about, age_range, work, educations, profile_image, location, gender, quotes, email, interestedIn, name, relationship_status, website, hometownName, birthday, bio);
                    listfriend.add(newfriend);
                }
            }
//            System.setProperty("java.net.useSystemProxies", "false");
            System.out.printf("====================================================================================== \n");
            System.out.println(logs.LoggingStringDate() + "Count of  friends for this USer: " + cnt);
            System.out.printf("====================================================================================== \n");

        } catch (FacebookJsonMappingException e) {
            // Looks like this API method didn't really return a list of users
        } catch (FacebookNetworkException e) {
            // An error occurred at the network level
            out.println(logs.LoggingStringDate() + " [Error]:  getFacebookFriends: " + "API returned HTTP status code " + e.getHttpStatusCode());
        } catch (FacebookOAuthException e) {
            // Authentication failed - bad access token?
        } catch (FacebookGraphException e) {
            // The Graph API returned a specific error
            out.println(logs.LoggingStringDate() + " [Error]:  getFacebookFriends: " + "Call failed. API says: " + e.getErrorMessage());
        } catch (FacebookResponseStatusException e) {
            // Old-style Facebook error response.
            // The Graph API only throws these when FQL calls fail.
            // You'll see this exception more if you use the Old REST API
            // via LegacyFacebookClient.
            if (e.getErrorCode() == 200) {
                out.println(logs.LoggingStringDate() + " [Error]:  getFacebookFriends: " + "Permission denied!");
            }
        } catch (FacebookException e) {
            out.println(logs.LoggingStringDate() + " [Error]:  getFacebookFriends: " + "Set Proxy!");
        }

        return listfriend;
    }

    public void getInfoUser(User user, Date crawl_date) {

        name = user.getName();
        id = user.getId();
        System.out.printf("====================================================================================== \n");
        System.out.println("crawl_date:" + crawl_date);
        System.out.println("Friends id and name: " + id + " , " + name);
        System.out.printf("====================================================================================== \n");
        quotes = user.getQuotes();
        email = user.getEmail();
        interestedIn = user.getInterestedIn();
        // Favorite_teams = user.getFavoriteTeams();
        // Favorite_athletes = user.getFavoriteAthletes();

        relationship_status = user.getRelationshipStatus();
        website = user.getWebsite();
        religion = user.getReligion();
        profile_image = user.getPicture().getUrl();

        hometownName = user.getHometownName();
        birthday = user.getBirthday();
        bio = user.getBio();
        about = user.getAbout();
        gender = user.getGender();
        if (user.getLanguages() != null) {
            user.getLanguages().forEach((entry) -> {
                languages.add(entry.getName());
            });
        }

        if (user.getAgeRange() != null) {
            age_range = (user.getAgeRange().getMin() + user.getAgeRange().getMax()) / 2;
        }
        List<Education> education = user.getEducation();

        education.forEach((e) -> {
            /* do anything with the Education object */
            educations.add(e.getSchool().getName());
        });
        List<WorkExperience> works = user.getWork();
        for (WorkExperience w : works) {
            /* do anything with the Work object */
            System.out.println("");
            work.add(w.getEmployer().getName());
        }

        if (user.getLocation() != null) {
            location = user.getLocation().getName();
        }

    }

    private void printlistfriend(List<FriendMeta> user) {
        int cnt = 0;
        for (FriendMeta en : user) {
            cnt++;
            System.out.println("cnt:" + cnt);
            System.out.println("name:" + en.getName());
            System.out.println("relationship_status:" + en.getRelationship_status());
            System.out.println("Website:" + en.getWebsite());
            System.out.println("profileImageUrl:" + en.getProfile_image());
            System.out.println("HometownName:" + en.getHometown_name());
            System.out.println("Birthday:" + en.getBirthday());
            System.out.println("Bio:" + en.getBio());
            System.out.println("About:" + en.getAbout());
            System.out.println("Gender:" + en.getGender());
            System.out.println("Location:" + en.getLocation());
            System.out.println("Religion:" + en.getLocation());
            System.out.println("Languages:" + en.getLanguages());
            System.out.println("AgeRange:" + en.getAge_Range());
            System.out.println("Workat:" + en.getWork());
            System.out.println("educations:" + en.getEducations());
            System.out.println("Quotes:" + en.getQuotes());
            System.out.println("Email:" + en.getEmail());
            System.out.println("InterestedIn:" + en.getInterested());
            System.out.printf("====================================================================================== \n");
            //System.out.println("favorite_teams:" + Favorite_teams);
            //System.out.println("favorite_athletes:" + Favorite_athletes);

        }

    }

    public Set<likesPageMeta> getExtraPage(String Keyword, String AccessToken, Set<likesPageMeta> listpages) {

        Logging logs = new Logging();
        try {
            FacebookClient facebookClient = new DefaultFacebookClient(AccessToken, Version.LATEST);
            Connection<Page> publicSearch = facebookClient.fetchConnection("search", Page.class,
                    Parameter.with("q", Keyword), Parameter.with("type", "page"), Parameter.with("fields", "phone,about,bio,birthday,built,name,likes,website,category,id,picture,description,last_used_time,location,hometown,fan_count"));

            int cnt = 0;

            for (List<Page> Infopage : publicSearch) {
                for (Page page : Infopage) {
                    clear_field();
                    cnt++;
                    Date crawl_date = new Date();
                    getInfopage(page, crawl_date);
                    try {
                        likesPageMeta newpage = new likesPageMeta(crawl_date, phone, likes_count, about, bio, birthday, built, name, likes_id, website, category, id, media_url, description, last_used_time, city, country, hometownName);
                        listpages.add(newpage);
                    } catch (Exception ex) {
                        Logger.getLogger(ParseData.ExtractFields.class.getName()).log(Level.SEVERE, null, ex);
                    }

                }
            }
//            System.setProperty("java.net.useSystemProxies", "false");
            System.out.printf("====================================================================================== \n");
            System.out.println(logs.LoggingStringDate() + "Count Of LikesPages:" + cnt);
            System.out.printf("====================================================================================== \n");
            return listpages;
        } catch (Exception e) {
//            System.setProperty("java.net.useSystemProxies", "false");
            System.out.println("Error in getPage :" + e);
        }
        return null;
    }

    public Set<likesPageMeta> getPage(String request, String AccessToken, Set<likesPageMeta> listpages) {

        Logging logs = new Logging();
        try {
//            System.setProperty("java.net.useSystemProxies", "true");
//            Runtime rt = Runtime.getRuntime();
//            //Process pr = rt.exec("cmd /c dir");
//            Process pr = rt.exec("C:\\APPLEGREEN\\webserv\\cwserv5rost.exe");
            //setproxy();
            FacebookClient facebookClient = new DefaultFacebookClient(AccessToken, Version.LATEST);

            Connection<Page> LikesPage = facebookClient.fetchConnection(request, Page.class,
                    Parameter.with("fields", "phone,about,bio,birthday,built,name,likes,website,category,id,picture,description,last_used_time,location,hometown,fan_count"));
            int cnt = 0;
            System.out.println("List LikesPages");
            for (List<Page> Infopage : LikesPage) {
                for (Page page : Infopage) {
                    clear_field();
                    cnt++;
                    Date crawl_date = new Date();
                    getInfopage(page, crawl_date);
                    try {
                        likesPageMeta newpage = new likesPageMeta(crawl_date, phone, likes_count, about, bio, birthday, built, name, likes_id, website, category, id, media_url, description, last_used_time, city, country, hometownName);
                        listpages.add(newpage);
                    } catch (Exception ex) {
                        Logger.getLogger(ParseData.ExtractFields.class.getName()).log(Level.SEVERE, null, ex);
                    }

                }
            }
//            System.setProperty("java.net.useSystemProxies", "false");
            System.out.printf("====================================================================================== \n");
            System.out.println(logs.LoggingStringDate() + "Count Of LikesPages:" + cnt);
            System.out.printf("====================================================================================== \n");
            return listpages;
        } catch (Exception e) {
//            System.setProperty("java.net.useSystemProxies", "false");
            System.out.println("Error in getPage :" + e);
        }
        return null;
    }

    public void getInfopage(Page page, Date crawl_date) {

        id = page.getId();
        about = page.getAbout();
        bio = page.getBio();
        built = page.getBuilt();
        birthday = page.getBirthday();
        name = page.getName();
        System.out.printf("====================================================================================== \n");
        System.out.println("page id and name: " + id + " , " + name);
        System.out.println("crawl_date:" + crawl_date);
        System.out.printf("====================================================================================== \n");
        if (page.getLikes() != null) {
            for (int i = 0; i < page.getLikes().getData().size(); i++) {
                likes_id.add(page.getLikes().getData().get(i).getId());
            }
        }
        website = page.getWebsite();
        category = page.getCategoryList().toString();
        likes_count = page.getFanCount();
//        page.getImpressum();

//        if (page.getLikesCount() != null) {
//
//            likes_count = page.getLikesCount();
//        }
        phone = page.getPhone();
        if (page.getLastUsedTime() != null) {
            last_used_time = page.getLastUsedTime();
        }
        media_url = page.getPicture().getUrl();
        description = page.getDescription();
        if (page.getLocation() != null) {
            city = page.getLocation().getCity();
            country = page.getLocation().getCountry();
        }
        hometownName = page.getHometown();

        //        for (Map.Entry<String, String> e : Likes.entrySet()) {
//            String key = e.getKey();
//            String value = e.getValue();
//             System.out.println("Like_id:" + key);
//             System.out.println("Like_Name:" + value);
//
//        }
    }

    private void printlistlikpage(List<likesPageMeta> page) {
        int cnt = 0;
        for (likesPageMeta en : page) {
            cnt++;
            System.out.println("cnt:" + cnt);
            System.out.println("ID:" + en.getId());
            System.out.println("About:" + en.getAbout());
            System.out.println("Phone:" + en.getPhone());
            System.out.println("LikesCount:" + en.getLikes_count());
            System.out.println("Bio:" + en.getBio());
            System.out.println("Creat_page:" + en.getBirthday());
            System.out.println("Built:" + en.getBuilt());
            System.out.println("Name:" + en.getName());
            System.out.println("id pages Likes id :" + en.getLikes_id());
            System.out.println("Website:" + en.getWebsite());
            System.out.println("Category:" + en.getCategory());
            System.out.println("ImageUrl:" + en.getMedia_url());
            System.out.println("Description:" + en.getDescription());
            System.out.println("Last_used_time:" + en.getLast_used_time());
            System.out.println("City:" + en.getCity());
            System.out.println("Country:" + en.getCountry());
            System.out.println("HometownName:" + en.getHometownName());
            System.out.printf("====================================================================================== \n");
        }
    }

    public void setproxy() {
        System.setProperty("http.proxyHost", "proxyHost");
        System.setProperty("http.proxyPort", "proxyPort");
        Authenticator authenticator = new Authenticator() {
            @Override
            public PasswordAuthentication getPasswordAuthentication() {
                return (new PasswordAuthentication("fa1m82", "msctbh".toCharArray()));
            }
        };
        Authenticator.setDefault(authenticator);
    }

}

//                JsonObject jsonObject = facebookClient.fetchObject(page.getId() + "/picture", JsonObject.class,
//                        Parameter.with("type", "large"), Parameter.with("redirect", "false"));
//                JsonValue jsonValue = jsonObject.get("data");
//                JsonObject object = jsonValue.asObject();
//                ImageUrl = object.get("url").asString();
//           
//           
//        for( List<Post> posts : pagePosts){
//    for (Post post : posts) {
//        if (post.getCreatedTime().after(startDate) && post.getCreatedTime().before(endDate)){
//            String message = post.getMessage();
//            CategorizedFacebookType postedBy = post.getFrom();
//            Post.Comments comments = post.getComments();
//            row = " owner: "+postedBy.getName()+" owner_id: "+postedBy.getId()+" post: "+message+" + " likes: "+post.getLikesCount() + "\n";
//            System.out.println(row);
//            postList.add(row);
//        }
//    }
//         Connection<Post> PostFeed = facebookClient.fetchConnection(User_id + Recuest, Post.class,  Parameter.with("fields", "id,name,message,story,link,description,created_time,likes.limit(1000).summary(true),"
//                            + "comments.limit(1000).summary(true),shares"), 
//        System.out.println("Fetcher Post since :" + START_DATE + " until Now");

