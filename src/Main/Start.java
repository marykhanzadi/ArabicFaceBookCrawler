/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Main;

import Crawler.Fetcher;
import Processing.DirectoryConfigs;
import Processing.Logging;
import Solr.SolrIndexConfig;
import java.net.URISyntaxException;

/**
 *
 * @author Jwan
 */
public class Start {

    public DirectoryConfigs confs;
    static String AccessTokenPost = "EAACW5Fg5N2IBAM0CUZCkEiblnNvxIDHh20IAYXO38CWgzIazOWs7kAAKMOOemV1i2rYkTTkX40QCSPLBk9OUWLMgBb0Gx9iTi7EQq33HXZBZCcEXZAgZATusmPQOMIMDK9UcRblHkk94YF1XLsr3U6ZCQJsPgKGN2fgIVTV8DTgAZDZD";
    //static String AccessTokenPost = "EAACW5Fg5N2IBAP1JNY84waxfkoaTgoOKS5bpy2iEluA9mysTscXE7O0mxkBRiJHxGNbrIiI67066V12QOTuevZCln5RRXbXLA7c2qnTSEXrjIFK00zmk4eOhrA8ZBiuolt7lJE9Rz3qXDfMTYN5MaqR7jhZC5nPS23rkxBfqQZDZD";

    public static void main(String[] args) throws URISyntaxException, InterruptedException {
        // Set Proxy
        setproxy();
        Logging logs = new Logging();

        Start st = new Start();

        if (args == null || args.length == 0) {
            st.confs = new DirectoryConfigs(DirectoryConfigs.getCurrentPath() + "configs.prop");//خواندن اطلاعات داخل کانفیک
        } else {
            st.confs = new DirectoryConfigs(args[0]);
        }

        SolrIndexConfig indexer = new SolrIndexConfig();// پیدا کردن کور داخل سرور وخواندن سایتها با ایدیاشون ازفایل سورس ایدی 
        indexer.IndexerInitializaton(st.confs);
        Fetcher fetcher = new Fetcher(st.confs);
        fetcher.RunExtractTimer();

    }

    public static void setproxy() {
        System.setProperty("http.proxyHost", "18.217.218.126");
        System.setProperty("http.proxyPort", "3128");
        System.setProperty("https.proxyHost", "45.63.70.98");
        System.setProperty("https.proxyPort", "8080");
    }
}
