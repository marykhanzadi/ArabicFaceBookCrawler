/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Crawler;

import CrawlerIO.Reader;
import ParseData.ExtractFields;
import Processing.DirectoryConfigs;
import Processing.FriendMeta;
import Processing.Logging;
import Processing.PostMeta;
import Processing.likesPageMeta;
import Solr.SolrIndexConfig;
import Solr.SolrTrends;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Jwan
 */
public class Fetcher {

    private DirectoryConfigs confs;
    private Set<String> KeywordSearch;

    public Fetcher(DirectoryConfigs configs) {
        confs = configs;

    }

    public void RunExtractTimer() throws InterruptedException {
        Timer timer = new Timer();
        long interval_friend = 1000 * 60 * 480;// 120;//Execute Java Timer at every 8 hours
        long interval_likepage = 1000 * 60 * 60 * 24;// 120;//Execute Java Timer at every 8 hours
        long interval_post = 1000 * 60 * 15;// 120;//Execute Java Timer at every 2 hours
        long interval_update_post = 1000 * 60 * 300;
        //fetchindex_friends();
        //timer.schedule(new FetcherTimer_friends(), 0, interval_friend);
        //Thread.sleep(70000);
        fetchindex_likePages();
        timer.schedule(new FetcherTimer_likePages(), 0, interval_likepage);
        Thread.sleep(100000);

        timer.schedule(new FetcherTimer_Post(45), 0, interval_post);
        Thread.sleep(120000);
        timer.schedule(new FetcherTimer_Post(24 * 60), 0, interval_update_post);
//    }
    }

    private void fetchindex_friends() {
        Logging logs = new Logging();
        try {
            System.out.println("======================================================================================\n"
                    + logs.LoggingStringDate() + " Begin Fetcher list friends");
            ExtractFields parser = new ExtractFields();
            List<FriendMeta> facebookFriends = parser.getFacebookFriends("me", confs.AccessTokenPost);
            //index to solr
            //SolrIndexConfig indexer = new SolrIndexConfig();
            // indexer.ThreadIndexing_friends(facebookFriends);

        } catch (Exception ex) {
            System.err.println(logs.LoggingStringDate() + " [Error]:  fetchindex_friends: " + ex);
            ex.printStackTrace();
        }
    }

    class FetcherTimer_friends extends TimerTask {

        @Override
        public void run() {
            fetchindex_friends();
        }
    }

    class FetcherTimer_likePages extends TimerTask {

        @Override
        public void run() {

            try {
                fetchindex_likePages();
            } catch (InterruptedException ex) {
                Logger.getLogger(Fetcher.class.getName()).log(Level.SEVERE, null, ex);
            }
            Index_generatepage(confs);
        }
    }

    private void Index_generatepage(DirectoryConfigs configs) {
        Logging logs = new Logging();
        confs = configs;
        Reader rd = new Reader();
        KeywordSearch = rd.ReadFileList(confs.keywordSearch);//read Regex url from urls_filter.txt
        ExtractFields parser = new ExtractFields();
        for (String kwSearch : KeywordSearch) {
            try {
                System.out.println("======================================================================================\n"
                        + logs.LoggingStringDate() + " Begin Fetcher list LikePages where  KeywordSearch for name page==" + kwSearch);
                Set<likesPageMeta> facebookpages = new HashSet<>();
                facebookpages = parser.getExtraPage(kwSearch, confs.AccessTokenPost, facebookpages);
                System.out.println("Total count Of  Get Post pagelikes where  KeywordSearch for name page==" + kwSearch + "Is: " + facebookpages.size());
                //index tom solr
                SolrIndexConfig indexer = new SolrIndexConfig();
                indexer.ThreadIndexing_likesPage(facebookpages);
            } catch (Exception ex) {
                System.err.println(logs.LoggingStringDate() + " [Error]: fetchindex_likePages");
                ex.printStackTrace();
            }
        }

    }

    class FetcherTimer_Post extends TimerTask {

        int review_time;

        public FetcherTimer_Post(int time) {
            this.review_time = time;
        }

        @Override
        public void run() {

            try {
                fetchindex_post(review_time);
            } catch (InterruptedException ex) {
                Logger.getLogger(Fetcher.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private Set<likesPageMeta> addOtherpages(Set<likesPageMeta> listpages) {
        Logging logs = new Logging();
        int totalcnt = 0;
        try {
            System.out.println("======================================================================================\n"
                    + logs.LoggingStringDate() + " get list id , name likes page from solr to add index fb_arabic_page ");
            SolrIndexConfig indexer = new SolrIndexConfig();
            SolrTrends trends = new SolrTrends(confs);
            ConcurrentHashMap<String, String> listpagesolar = new ConcurrentHashMap<>();
            trends.ExtractTop_likesid();  //get like_id   of page likes in indexer solar
            listpagesolar = trends.getPages();
            System.out.println("Total count Of  page id from solar is:=" + listpagesolar.size());
            ExtractFields parser = new ExtractFields();
            for (Map.Entry<String, String> next : listpagesolar.entrySet()) {//خواندن یکی یکی پیج ها و گرفتن اطلاعات پروفایل از هر کدام
                String idpage = next.getKey();
                listpages = parser.getPage(idpage, confs.AccessTokenPost, listpages);

            }
            return listpages;
        } catch (Exception ex) {
            System.err.println(logs.LoggingStringDate() + " [Error]: addOtherpages");
        }
        return null;
    }

    private void fetchindex_likePages() throws InterruptedException {
        Logging logs = new Logging();
        try {

            System.out.println("======================================================================================\n"
                    + logs.LoggingStringDate() + " Begin Fetcher list LikePages");
            ExtractFields parser = new ExtractFields();
            Set<likesPageMeta> facebookpages = new HashSet<>();
            facebookpages = parser.getPage("me/likes", confs.AccessTokenPost, facebookpages);
            //add like page from another pages
            //facebookpages = addOtherpages(facebookpages);
            System.out.println("Total count Of  Get  page likes=" + facebookpages.size());
            System.out.println(logs.LoggingStringDate() + "Fetcher likespage Done!");

            //index tom solr
            SolrIndexConfig indexer = new SolrIndexConfig();
            indexer.ThreadIndexing_likesPage(facebookpages);
        } catch (Exception ex) {
            System.err.println(logs.LoggingStringDate() + " [Error]: fetchindex_likePages");
            ex.printStackTrace();
        }
    }

    private void fetchindex_post(int time) throws InterruptedException {
        Logging logs = new Logging();
        try {
            int totalcnt = 0;
            ExtractFields parser = new ExtractFields();
            Calendar calendar = Calendar.getInstance();
            String End_Date = calendar.getTime().toString();
            calendar.add(Calendar.MINUTE, -time);
            String Start_Date = calendar.getTime().toString();
            totalcnt = 0;
            System.out.println("======================================================================================\n"
                    + logs.LoggingStringDate() + " get list id , name likes page from solr ");
            SolrIndexConfig indexer = new SolrIndexConfig();
            SolrTrends trends = new SolrTrends(confs);
            ConcurrentHashMap<String, String> facebookpages = new ConcurrentHashMap<>();
            trends.ExtractTopIdPagesLike();  //get list id , name of page likes in indexer solar
            facebookpages = trends.getPages();
            for (Map.Entry<String, String> next : facebookpages.entrySet()) {//خواندن یکی یکی پیج ها و گرفتن پست از هر کدام
                String idpage = next.getKey();
                System.out.println("======================================================================================\n"
                        + logs.LoggingStringDate() + " Begin Fetcher Post of pages" + idpage + "since " + Start_Date + " until Now ");
                List<PostMeta> timelinePost = parser.getTimelinePost(idpage, "/feed", confs.AccessTokenPost, Start_Date, End_Date, "Page");// گرفتن پست از پیج
                indexer.ThreadIndexing_post(timelinePost);//ایندکس پست در سولار 
                totalcnt += timelinePost.size();
            }

            System.out.println(logs.LoggingStringDate() + "Fetcher Post of LikePages Done!");
            System.out.println("Total count Of  Get Post pagelikes=" + totalcnt);
        } catch (Exception ex) {
            System.err.println(logs.LoggingStringDate() + " [Error]: fetchindex_post");
            ex.printStackTrace();
        }
    }

}

//                parser.getFacebookFriends("me", confs.AccessTokenPost); // parser.getTimelinePost("me", "/home", confs.AccessTokenPost, dateStart);
//parser.getTimelinePost("me", "/feed", confs.AccessTokenPost, Start_Date, End_Date);
//                 long ms_date = new Date().getTime();
//                verified_ID.put(en.getUri().trim(), ms_date);
//                visited_ID.put(en.getUri().trim(), true);
//                List<FriendMeta> facebookpages = parser.getPage("me/likes", confs.AccessTokenPost);
//
//                System.out.println("======================================================================================\n"
//                        + logs.LoggingStringDate() + " Begin Fetcher Post of page since " + Start_Date.toString() + " until Now ");
//                for (FriendMeta next : facebookpages) {
//                    List<FaceMeta> timelinePost = parser.getTimelinePost(next.ID, "/feed", confs.AccessTokenPost, Start_Date, End_Date, "Page");
//                    totalcnt += timelinePost.size();
//                }
//                System.out.println("Total count Of  Get Post pagelikes" + totalcnt);
//                String startTime = "2013-12-21T18:30:00+0100";
//                SimpleDateFormat incomingFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
//                Date date = incomingFormat.parse(startTime);
//
//                SimpleDateFormat outgoingFormat = new SimpleDateFormat(" EEEE, dd MMMM yyyy", java.util.Locale.getDefault());
//
//                System.out.println(outgoingFormat.format(date));
