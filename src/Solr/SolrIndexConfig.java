package Solr;

import static Solr.FindSolrServer.GetCores;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import Processing.DirectoryConfigs;
import Processing.Logging;
import Processing.FriendMeta;
import Processing.PostMeta;
import Processing.likesPageMeta;
import java.util.Set;
import org.apache.solr.client.solrj.impl.HttpSolrClient;

/**
 *
 * @author Hamed Fakour
 */
public final class SolrIndexConfig {

    public DirectoryConfigs dir_configs;
    //private static int thread_count;
    public static HttpSolrClient indexerFriends;
    public static HttpSolrClient indexerLikespage;
    public static HttpSolrClient indexerPost;

    public void IndexerInitializaton(DirectoryConfigs configs) {
        dir_configs = configs;
        indexerFriends = new HttpSolrClient(configs.ServerUrl + configs.facefriends);
        indexerLikespage = new HttpSolrClient(configs.ServerUrl + configs.facelikes);
        indexerPost = new HttpSolrClient(configs.ServerUrl + configs.facepost);
        //FindSolrServer.LoadAllCores(configs);
        //CoreAdmin admin = new CoreAdmin();
        //admin.CreateOldCores(dir_configs);

        //Ngram_Noise = ReadChunkerFile(dir_configs.chunk_noise);
        //Ngram_Bounds = ReadChunkerFile(dir_configs.chunk_bound);
        RunOptimizeCores();//زمان کراول
        //thread_count = 0;
    }

    public void RunOptimizeCores() {
        Timer timer = new Timer();
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 0);
        timer.schedule(new OptimizeCores(), new Date(cal.getTimeInMillis()), 4 * 24 * 60 * 60 * 1000); //per one day
    }



    public void ThreadIndexing_post(List<PostMeta> post_data) throws InterruptedException {
//        if(thread_count > 4) {
//            Thread.sleep(3000);
//            thread_count--;
//        }
        Logging logs = new Logging();
        //Month Cores
        Thread threads_month = new Thread(()
                -> {
            Random r = new Random();
            int version = r.nextInt(1000);
            System.out.println(logs.LoggingStringDate() + " Start  Post Indexer {" + version + "}");

            SolrIndexer_Post indexer = new SolrIndexer_Post();
            indexer.AddDocuments(post_data, dir_configs);
            System.out.println(logs.LoggingStringDate() + " Add naumer of Post " + post_data.size() + " Indexer {" + version + "}");
            System.out.println(logs.LoggingStringDate() + " Done Post Indexer {" + version + "}");
        });
        threads_month.start();
//        thread_count++;
    }

    public void ThreadIndexing_likesPage(Set<likesPageMeta> likePage_data) throws InterruptedException {
//        if(thread_count > 4) {
//            Thread.sleep(3000);
//            thread_count--;
//        }
        Logging logs = new Logging();
        //Month Cores
        Thread threads_month = new Thread(()
                -> {
            Random r = new Random();
            int version = r.nextInt(1000);
            System.out.println(logs.LoggingStringDate() + " Start likespage Indexer {" + version + "}");

            SolrIndexer_likePage indexer = new SolrIndexer_likePage();
            indexer.AddDocuments(likePage_data, dir_configs);
            System.out.println(logs.LoggingStringDate() + " Add naumer of likePage_data " + likePage_data.size() + " Indexer {" + version + "}");
            System.out.println(logs.LoggingStringDate() + " Done likespage Indexer {" + version + "}");
        });
        threads_month.start();
//        thread_count++;
    }

    public void ThreadIndexing_friends(List<FriendMeta> friend_data) throws InterruptedException {
        Logging logs = new Logging();
        Thread threads_month = new Thread(()
                -> {
            Random r = new Random();
            int version = r.nextInt(1000);
             System.out.printf("====================================================================================== \n");
            System.out.println(logs.LoggingStringDate() + " Start friend Indexer {" + version + "}");
             System.out.printf("====================================================================================== \n");
            SolrIndexer_friends indexer = new SolrIndexer_friends();
            indexer.AddDocuments(friend_data);
             System.out.printf("====================================================================================== \n");
            System.out.println(logs.LoggingStringDate() + " Add naumer of friend " + friend_data.size() + " Indexer {" + version + "}");
             System.out.printf("====================================================================================== \n");
            System.out.println(logs.LoggingStringDate() + " Done friend Indexer {" + version + "}");
             System.out.printf("====================================================================================== \n");
        
        });
        threads_month.start();
//        thread_count++;
    }
    

    class OptimizeCores extends TimerTask {

        @Override

        public void run() {
            Logging logs = new Logging();
            System.out.println(logs.LoggingStringDate() + " Cores Optimization is running... ");
            CoreAdmin admin = new CoreAdmin();
            try {
                for (String key : FindSolrServer.GetCores().keySet()) {
                    admin.OptimizeCore(dir_configs, key);
                }
                System.out.println("Cores Optimization Done!");
            } catch (Exception ex) {
                System.err.println("Error: Cores Optimization ");
            }
        }
    }


}
