package Solr;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.common.SolrInputDocument;
import Processing.Logging;
import Processing.FriendMeta;

/**
 *
 * @author Hamed Fakour
 */
public class SolrIndexer_friends {

    public void AddDocuments(List<FriendMeta> news_list) {
        Logging logs = new Logging();
        List<FriendMeta> listfriend = new ArrayList(news_list);
        if (news_list.isEmpty()) {
            System.err.println("Urls list is empty!");
            return;
        }

        //FindSolrServer fs = new FindSolrServer(dir_conf);
        //HttpSolrClient indexer = fs.IndexerName_Month(news.get(0).publish_date);
        //HttpSolrClient indexer = new HttpSolrClient(dir_conf.ServerUrl + "arab_news");
        //System.setProperty("java.net.useSystemProxies", "false");
        for (FriendMeta nue : listfriend) {

            try {

                SolrInputDocument document = AddDocument(nue.getCrawl_date(), nue.getLanguages(), nue.getReligion(), nue.getId(), nue.getAbout(), nue.getAge_Range(), nue.getWork(), nue.getEducations(), nue.getProfile_image(), nue.getLocation(), nue.getGender(), nue.getQuotes(), nue.getEmail(), nue.getInterestedIn(), nue.getName(), nue.getRelationship_status(), nue.getWebsite(), nue.getHometown_name(), nue.getBirthday(), nue.getBio());
                SolrIndexConfig.indexerFriends.add(document);

                /*long diff = nue.publish_date.getTime() - SolrIndexConfig.GetLastStartDate().getTime();
                if (diff < 0) {
                    indexer.commit(true, true);
                    indexer = fs.IndexerName_Month(nue.publish_date);
                }*/
            } catch (Exception ex) {
                System.err.println(logs.LoggingStringDate() + " [Error]: SolrIndexer_friends.AddDocuments => fb.com/ " + nue.getId());
                Logger.getLogger(SolrIndexer_friends.class.getName()).log(Level.SEVERE, null, ex);
                continue;
            }
        }
        try {
            SolrIndexConfig.indexerFriends.commit(false, true, true);
        } catch (SolrServerException | IOException ex) {
            System.err.println(logs.LoggingStringDate() + " [Error]: SolrIndexer_friends.AddDocuments => Indexer_friends.final_commit");
            Logger.getLogger(SolrIndexer_friends.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String getUTC_Date(Date date) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        return formatter.format(date);
    }

    protected SolrInputDocument AddDocument(Date crawl_date, List languages, String religion, String id, String about, int ageRange, List work, List educations, String profile_image, String location, String gender, String quotes, String email, List interested, String name, String relationship_status, String website, String hometown_name, String birthday, String bio) {
        SolrInputDocument doc = new SolrInputDocument();
        // PythonArabicNormalizer pan = new PythonArabicNormalizer();
        doc.addField("id", id);
        doc.addField("languages", languages);
        doc.addField("religion", religion);
        doc.addField("about", about);
        doc.addField("age_range", ageRange);
        doc.addField("work", work);
        doc.addField("educations", educations);
        doc.addField("profile_image", profile_image);
        doc.addField("location", location);
        doc.addField("gender", gender);
        doc.addField("quotes", quotes);
        doc.addField("email", email);
        doc.addField("interested", interested);
        doc.addField("name", name);
        doc.addField("relationship_status", relationship_status);
        doc.addField("website", website);
        doc.addField("hometown_name", hometown_name);
        doc.addField("birthday", birthday);
        doc.addField("bio", bio);
        doc.addField("crawl_date", getUTC_Date(crawl_date));

        return doc;
    }

}
