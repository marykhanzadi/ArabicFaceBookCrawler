
package Solr;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.request.CoreAdminRequest;
import org.apache.solr.client.solrj.response.CoreAdminResponse;
import org.apache.solr.common.params.CoreAdminParams.CoreAdminAction;
import Processing.DirectoryConfigs;

/**
 *
 * @author Hamed Fakour
 */
public class FindSolrServer {
    
    public static String solr_server;
    public static HashMap<String, HttpSolrClient> Cores;
    public static DirectoryConfigs cor_dir;
    
    public static synchronized HashMap<String, HttpSolrClient> GetCores(){
        return Cores;
    }
    
    public static void LoadAllCores(DirectoryConfigs dirs){
        cor_dir = dirs;//read core project
        solr_server = cor_dir.ServerUrl;
        Cores = new HashMap();
        try {
            CoreAdminRequest request = new CoreAdminRequest();
            request.setAction(CoreAdminAction.STATUS);
            CoreAdminResponse cores = request.process(new HttpSolrClient(solr_server));
            for (int i = 0; i < cores.getCoreStatus().size(); i++) {
                String name = cores.getCoreStatus().getName(i);
                if(name.startsWith("fb_")){
                    Cores.put(name, new HttpSolrClient(solr_server + name));
                }
            }
        } catch (SolrServerException | IOException ex) { }
    }
    

    
    
    
}