
package Solr;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.FileUtils;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.request.CoreAdminRequest;
import org.apache.solr.client.solrj.response.CoreAdminResponse;
import org.apache.solr.common.util.NamedList;
import Processing.DirectoryConfigs;

/**
 *
 * @author Hamed Fakour
 */
public class CoreAdmin {
    
    public void UnloadCore(String SolrURL, String index_name){
        HttpSolrClient solr = new HttpSolrClient(SolrURL);
        try {
            CoreAdminRequest.unloadCore(index_name, true, false, solr);
        } catch (SolrServerException | IOException ex) {
            Logger.getLogger(CoreAdmin.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("Index Core deleted.");
    }
    
    
    public void ReloadCore(String SolrURL, String index_name){
        HttpSolrClient solr = new HttpSolrClient(SolrURL);
        try {
            CoreAdminRequest.reloadCore(index_name, solr);
        } catch (SolrServerException | IOException ex) {
            Logger.getLogger(CoreAdmin.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    
   
    
    
    public void OptimizeCore(DirectoryConfigs dirs, String core_name) throws Exception 
    {
        HttpSolrClient solrServer = new HttpSolrClient(dirs.ServerUrl + core_name);
        CoreAdminResponse coreAdminResponse = CoreAdminRequest.getStatus(core_name, dirs.Solr);
        NamedList<Object> index = (NamedList<Object>) coreAdminResponse.getCoreStatus(core_name).get("index");
        Integer deletedDocs = (Integer) index.get("deletedDocs");
        //Integer segmentCount = (Integer) index.get("segmentCount");
        //Integer numDocs = (Integer) index.get("numDocs");
        //Integer maxDoc = (Integer) index.get("maxDoc");
        if(deletedDocs > 300){
            System.out.println("Core Optimization: Core Name ===>> " + core_name);
            solrServer.optimize(true, true);
        }
    }

    
    
    public void CreateIndexDir(String DefaultIndexDir, String CoreDir){
        try {
            File core_dir = new File(CoreDir);
            if (!core_dir.exists()) {
                core_dir.mkdir();
                new File(core_dir+"/conf").mkdir();
                new File(core_dir+"/data").mkdir();
            }
            FileUtils.copyDirectory(new File(DefaultIndexDir), new File(CoreDir+"/conf"));
        } catch (IOException ex) { }
    }
    
    
      
    
}
