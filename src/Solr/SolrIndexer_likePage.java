package Solr;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.common.SolrInputDocument;
import Processing.DirectoryConfigs;
import Processing.Logging;
import Processing.likesPageMeta;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 * @author Hamed Fakour
 */
public class SolrIndexer_likePage {

    public void AddDocuments(Set<likesPageMeta> news_list, DirectoryConfigs dir_conf) {
        Logging logs = new Logging();
        List<likesPageMeta> listlikepage = new ArrayList(news_list);
        if (news_list.isEmpty()) {
            System.err.println("Urls list is empty!");
            return;
        }

        for (likesPageMeta nue : listlikepage) {
            try {

                SolrInputDocument document = AddDocument(nue.getCrawl_date(), nue.getPhone(), nue.getLikes_count(), nue.getAbout(), nue.getBio(), nue.getBirthday(), nue.getBuilt(), nue.getName(), nue.getLikes_id(), nue.getWebsite(), nue.getCategory(), nue.getId(), nue.getMedia_url(), nue.getDescription(), nue.getLast_used_time(), nue.getCity(), nue.getCountry(), nue.getHometownName());
                SolrIndexConfig.indexerLikespage.add(document);

            } catch (Exception ex) {
                System.err.println(logs.LoggingStringDate() + " [Error]: SolrIndexer.AddDocuments => fb.com/ " + nue.getId());
                Logger.getLogger(SolrIndexer_likePage.class.getName()).log(Level.SEVERE, null, ex);
                continue;
            }
        }
        try {
            SolrIndexConfig.indexerLikespage.commit(false, true, true);
        } catch (SolrServerException | IOException ex) {
            System.err.println(logs.LoggingStringDate() + " [Error]: SolrIndexer.AddDocuments => indexer.final_commit");
            Logger.getLogger(SolrIndexer_likePage.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    protected SolrInputDocument AddDocument(Date crawl_date, String phone, long likes_count, String about, String bio, String birthday, String built, String name, List<String> likes_id, String website, String category, String id, String media_url, String description, Date last_used_time, String city, String country, String hometown_name) throws IOException, SolrServerException, Exception //
    {
        SolrInputDocument doc = new SolrInputDocument();
        
        doc.addField("phone", phone);
        doc.addField("about", about);
        doc.addField("likes_count", likes_count);
        doc.addField("bio", bio);
        doc.addField("birthday", birthday);
        doc.addField("built", built);
        doc.addField("name", name);
        doc.addField("likes_id", likes_id);
        doc.addField("website", website);
        doc.addField("category", category);
        doc.addField("id", id);
        doc.addField("media_url", media_url);
        doc.addField("description", description);
        doc.addField("last_used_time", last_used_time);
        doc.addField("city", city);
        doc.addField("country", country);
        doc.addField("hometown_name", hometown_name);
         doc.addField("crawl_date", getUTC_Date(crawl_date));
        return doc;
    }

    public String getUTC_Date(Date date) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        return formatter.format(date);
    }

}
