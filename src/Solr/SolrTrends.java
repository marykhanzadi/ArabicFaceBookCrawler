package Solr;

import CrawlerIO.Reader;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.util.NamedList;
import Processing.DirectoryConfigs;
import Processing.Logging;
import static com.restfb.json.Json.object;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import org.apache.solr.common.SolrDocumentList;

/**
 *
 * @author h.fakour
 */
public class SolrTrends {

    private DirectoryConfigs config;
    private static ConcurrentHashMap<String, String> Pages;

    public SolrTrends(DirectoryConfigs config) {
        this.config = config;
        Pages = new ConcurrentHashMap();
    }

    public ConcurrentHashMap<String, String> getPages() {
        return Pages;
    }

    public static void ClearPages() {
        Pages.clear();
    }

    public static void main(String[] args) {
        DirectoryConfigs config = new DirectoryConfigs("configs.prop");
        SolrTrends st = new SolrTrends(config);
        st.ExtractTopIdPagesLike();
    }


 public void ExtractTop_likesid() {
       // System.setProperty("java.net.useSystemProxies", "false");
        SolrQuery query = new SolrQuery();
        query.setQuery("*:*");
        query.setRows(Integer.MAX_VALUE);
        query.set("fl", "likes_id");
        Date now = new Date();
        SolrIndexer_likePage si = new SolrIndexer_likePage();
        String now_utc = si.getUTC_Date(now);
        Calendar cal = Calendar.getInstance();
        cal.setTime(now);
        cal.add(Calendar.HOUR_OF_DAY, -8);
        Date start = cal.getTime();
        String start_utc = si.getUTC_Date(start);
        //query.set("fq", "pub_date:[" + start_utc + " TO " + now_utc + "]");
        HttpSolrClient server = new HttpSolrClient(config.ServerUrl + config.facelikes);
        QueryResponse response = null;
        try {
            response = server.query(query);
        } catch (SolrServerException | IOException ex) {
            Logger.getLogger(SolrTrends.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (response.getResults().getNumFound() == 0) {
            return;
        }
        ClearPages();
        SolrDocumentList result = response.getResults();
        for (int i = 0; i < result.size(); i++) {
             // Object get = result.get(i).get("likes_id");
            if (result.get(i).get("likes_id") != null) {

                String[] split = result.get(i).get("likes_id").toString().split(",");
                for (int j = 0; j < split.length; j++) {
                    Pages.put(split[j].toString().trim(), " ");
                }
            }

        }

    }
    public void ExtractTopIdPagesLike() {
       // System.setProperty("java.net.useSystemProxies", "false");
        SolrQuery query = new SolrQuery();
        query.setQuery("*:*");
        query.setRows(Integer.MAX_VALUE);
        query.set("fl", "id,name,likes_id");
        Date now = new Date();
        SolrIndexer_likePage si = new SolrIndexer_likePage();
        String now_utc = si.getUTC_Date(now);
        Calendar cal = Calendar.getInstance();
        cal.setTime(now);
        cal.add(Calendar.HOUR_OF_DAY, -8);
        Date start = cal.getTime();
        String start_utc = si.getUTC_Date(start);
        //query.set("fq", "pub_date:[" + start_utc + " TO " + now_utc + "]");
        HttpSolrClient server = new HttpSolrClient(config.ServerUrl + config.facelikes);
        QueryResponse response = null;
        try {
            response = server.query(query);
        } catch (SolrServerException | IOException ex) {
            Logger.getLogger(SolrTrends.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (response.getResults().getNumFound() == 0) {
            return;
        }
        ClearPages();
        SolrDocumentList result = response.getResults();
        for (int i = 0; i < result.size(); i++) {
            Pages.put((String) result.get(i).get("id"), (String) result.get(i).get("name"));
            // Object get = result.get(i).get("likes_id");
//            if (result.get(i).get("likes_id") != null) {
//
//                String[] split = result.get(i).get("likes_id").toString().split(",");
//                for (int j = 0; j < split.length; j++) {
//                    Pages.put(split[j].toString().trim(), " ");
//                }
//            }

        }

    }
}




