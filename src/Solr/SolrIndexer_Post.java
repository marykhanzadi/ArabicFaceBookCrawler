package Solr;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.common.SolrInputDocument;
import Processing.DirectoryConfigs;
import Processing.Logging;
import Processing.PostMeta;
import java.util.Set;

/**
 *
 * @author Hamed Fakour
 */
public class SolrIndexer_Post {

    public void AddDocuments(List<PostMeta> news_list, DirectoryConfigs dir_conf) {
        Logging logs = new Logging();
        List<PostMeta> posts = new ArrayList(news_list);
        if (news_list.isEmpty()) {
            System.err.println("Urls list is empty!");
            return;
        }

        //FindSolrServer fs = new FindSolrServer(dir_conf);
        //HttpSolrClient indexer = fs.IndexerName_Month(news.get(0).publish_date);
        //HttpSolrClient indexer = new HttpSolrClient(dir_conf.ServerUrl + "arab_news");
        //System.setProperty("java.net.useSystemProxies", "false");
        for (PostMeta nue : posts) {
            try {

                SolrInputDocument document = AddDocument(nue.getCrawl_date(),nue.getPage_id(), nue.getFromName(), nue.getExternal_url(), nue.getType_attach(), nue.getTitle(), nue.getDescription(), nue.getCreated_time(), nue.getPost_message(), nue.getCaption(), nue.getLikes_count(), nue.getComments_count(), nue.getMedia_url(), nue.getType_message(), nue.getId(), nue.getHashtags(), nue.getMentions());
                SolrIndexConfig.indexerPost.add(document);

                /*long diff = nue.publish_date.getTime() - SolrIndexConfig.GetLastStartDate().getTime();
                if (diff < 0) {
                    indexer.commit(true, true);
                    indexer = fs.IndexerName_Month(nue.publish_date);
                }*/
            } catch (Exception ex) {
                System.err.println(logs.LoggingStringDate() + " [Error]: SolrIndexer.AddDocuments => fb.com/ " + nue.getId());
                Logger.getLogger(SolrIndexer_Post.class.getName()).log(Level.SEVERE, null, ex);
                continue;
            }
        }
        try {
            SolrIndexConfig.indexerPost.commit(false, true, true);
        } catch (SolrServerException | IOException ex) {
            System.err.println(logs.LoggingStringDate() + " [Error]: SolrIndexer.AddDocuments => indexer.final_commit");
            Logger.getLogger(SolrIndexer_Post.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    protected SolrInputDocument AddDocument(Date crawl_date,String page_id, String from_name, String external_url, String type_attach, String title, String description, Date created_time, String post_message, String caption, long like_count, int comments_count, String media_url, String type_message, String id, Set hashtags, Set mentions) {

        SolrInputDocument doc = new SolrInputDocument();
        doc.addField("page_id", page_id);
        doc.addField("id", id);
        doc.addField("from_name", from_name);
        doc.addField("external_url", external_url);
        doc.addField("type_attach", type_attach);
        doc.addField("title", title);
        doc.addField("description", description);
        doc.addField("created_time", created_time);
        doc.addField("post_message", post_message);
        doc.addField("caption", caption);
        doc.addField("like_count", like_count);
        doc.addField("comments_count", comments_count);
        doc.addField("media_url", media_url);
        doc.addField("type_message", type_message);
        doc.addField("hashtags", hashtags);
        doc.addField("mentions", mentions);
        doc.addField("crawl_date", getUTC_Date(crawl_date));
        return doc;
    }

    public String getUTC_Date(Date date) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        return formatter.format(date);
    }

}
