
package CrawlerIO;

import java.io.File;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author Hamed Fakour
 */
public class CrawlerFileManagment {
    
    public static void main(String[] args){
        long l = 1480594339841L;
        CrawlerFileManagment c = new CrawlerFileManagment();
        System.out.println(c.getFolderName(l));
        System.out.println(new Date().getTime());
    }
        
    public String getFolderName(long time) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time);
        return calendar.get(Calendar.YEAR) + "" + NumberFormat(calendar.get(Calendar.MONTH)+1)+
                "" + NumberFormat(calendar.get(Calendar.DAY_OF_MONTH));
    }
    
    
    public void IS_CreateFolder(String name){
        File core_dir = new File(name);
        if (!core_dir.exists()) {
            core_dir.mkdir();
        }
    }
        
    
    public String GetTodayDate(){
        Date now = new Date();
        return (now.getYear()+1900) + "" + NumberFormat(now.getMonth()+1) + "" + NumberFormat(now.getDate());
    }
    
    
    private String NumberFormat(int num){
        switch(num){
            case 1: return "01";
            case 2: return "02";
            case 3: return "03";
            case 4: return "04";
            case 5: return "05";
            case 6: return "06";
            case 7: return "07";
            case 8: return "08";
            case 9: return "09";
        }
        return num+"";
    }
    
}
