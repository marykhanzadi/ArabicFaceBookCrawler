
package CrawlerIO;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Map;

/**
 *
 * @author h.fakour
 */
public class Writer {

    
    public void WriteList(Map<String, Long> input_list, Map<String, Boolean> visited, String out_file){
        try {
            BufferedWriter out = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(out_file), "UTF-8"));
            for(Map.Entry<String, Long> t: input_list.entrySet()){ 
                if(visited.get(t.getKey())){
                    out.write(t.getKey() + "\t" + t.getValue() + "\n");
                }
            }
            out.flush();
            out.close();
        } catch (IOException ex) { }
    }
    
    
    public void WriteString(String text, String out_file){
        try {
            File file = new File(out_file);
            int i = 1;
            while(file.exists()){
                file = new File(out_file+"_"+i);
                i++;
            }
            BufferedWriter out = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(file), "UTF-8"));
            out.write(text);
            out.flush();
            out.close();
        } catch (IOException ex) { }
    }
    
    
    public void WriteList(Map<String, String> input_list, String out_file){
        try {
            BufferedWriter out = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(out_file,true), "UTF-8"));
            for(Map.Entry<String, String> t: input_list.entrySet()){ 
                out.write(t.getKey() + "\t" + t.getValue() + "\n");
            }
            out.flush();
            out.close();
        } catch (IOException ex) { }
    }

    
}
