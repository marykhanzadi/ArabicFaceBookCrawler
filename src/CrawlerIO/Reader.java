
package CrawlerIO;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author h.fakour
 */
public class Reader {
      
    public Set<String> ReadFileList(String file_dir){
        Set<String> lines = new HashSet<>();
        try {
            BufferedReader bf = new BufferedReader(new FileReader(file_dir));
            String line = "";
            while((line = bf.readLine()) != null){
                lines.add(line.replaceAll("[\uFEFF-\uFFFF]", ""));
            }
        } catch (IOException ex) {
            System.err.println("Error: Read File List");
        }
        return lines;
    }
    
    
    public LinkedHashMap<String, Integer> ReadFileMap(String file_dir){
        LinkedHashMap<String, Integer> lines = new LinkedHashMap();
        try {
            BufferedReader bf = new BufferedReader(new FileReader(file_dir));
            String line = "";
            while((line = bf.readLine()) != null){
                String[] sp = line.split("\t");
                lines.put(sp[0], Integer.parseInt(sp[1]));
            }
        } catch (IOException ex) { }
        return lines;
    }
    
    
    public List<String[]> ReadFileList_Split(String file_dir){
        List<String[]> lines = new ArrayList();
        try {
            BufferedReader bf = new BufferedReader(new FileReader(file_dir));
            String line = "";
            while((line = bf.readLine()) != null){
                lines.add(line.replaceAll("[\uFEFF-\uFFFF]", "").split("\t"));
            }
            bf.close();
        } catch (IOException ex) { }
        return lines;
    }
    
    
    public String ReadFileString(String file_dir){
        String output = "";
        try {
            BufferedReader bf = new BufferedReader(new FileReader(file_dir));
            String line = bf.readLine().replaceAll("[\uFEFF-\uFFFF]", "");
            output = line;
            while((line = bf.readLine()) != null){
                output += line + "\n";
            }
            bf.close();
        } catch (IOException ex) { }
        return output;
    }
    public String ReadInputStream(String file_dir){
        File in = new File(file_dir);
        String lines = "";
        java.util.Scanner s;
        try {
            s = new java.util.Scanner(in, "UTF-8");
            lines = s.next().replaceAll("[\uFEFF-\uFFFF]", "") + "\n";
            while(s.hasNextLine()){
                lines += s.nextLine() + "\n";
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Reader.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lines;
    }
    
    
    public Map<String, String> ReadFileMap_Split(String file_dir){
        Map<String, String> lines = new HashMap();
        try {
            BufferedReader bf = new BufferedReader(new FileReader(file_dir));
            String line = "";
            while((line = bf.readLine()) != null){
                String[] sp = line.replaceAll("[\uFEFF-\uFFFF]", "").split("\t");
                lines.put(sp[0], sp[1]);
            }
            bf.close();
        } catch (IOException ex) { }
        return lines;
    }
    
   
}
